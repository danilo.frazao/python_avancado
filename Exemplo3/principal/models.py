from decimal import Decimal

from django.db import models
from principal import managers

class ModelBase(models.Model):
    id = models.AutoField(
        null=False,
        primary_key=True,
    )

    active = models.BooleanField(
        null=False,
        default=True,
    )

    created_at = models.DateTimeField(
        null=False,
        auto_now_add=True,
    )

    modified_at = models.DateTimeField(
        null=False,
        auto_now=True,
    )

    class Meta:
        abstract = True
        managed = True

class Zone(ModelBase):
    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    class Meta:
        db_table = 'zone'

    def __str__(self):
        return self.name

class MaritalStatus(ModelBase):
    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    class Meta:
        db_table = 'marital_status'

    def __str__(self):
        return self.name

class Customer(ModelBase):
    class Gender(models.TextChoices):
        MALE = ('M', 'Male')
        FEMALE = ('F', 'Female')

    district = models.ForeignKey(
        null=False,
        to='District',
        on_delete=models.DO_NOTHING,
        db_column='id_district',
    )

    marital_status = models.ForeignKey(
        null=False,
        to='MaritalStatus',
        on_delete=models.DO_NOTHING,
        db_column='id_marital_status',
    )

    name = models.CharField(
        null=False,
        max_length=64,
    )

    income = models.DecimalField(
        null=False,
        max_digits=16,
        decimal_places=2,
    )

    gender = models.CharField(
        null=False,
        max_length=1,
        choices=Gender.choices,
    )

    class Meta:
        db_table = 'customer'

    def __str__(self):
        return self.name

class State(ModelBase):
    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    abbreviation = models.CharField(
        null=False,
        max_length=2,
        unique=True
    )

    class Meta:
        db_table = 'state'

    def __str__(self):
        return self.name

class City(ModelBase):
    state = models.ForeignKey(
        null=False,
        to='State',
        on_delete=models.DO_NOTHING,
        db_column='id_state',
    )

    name = models.CharField(
        null=False,
        max_length=64,
    )

    class Meta:
        db_table = 'city'

    def __str__(self):
        return self.name

class District(ModelBase):
    city = models.ForeignKey(
        null=False,
        to='City',
        on_delete=models.DO_NOTHING,
        db_column='id_city',
    )

    zone = models.ForeignKey(
        null=False,
        to='Zone',
        on_delete=models.DO_NOTHING,
        db_column='id_zone',
    )

    name = models.CharField(
        null=False,
        max_length=64,
    )

    class Meta:
        db_table = 'district'

    def __str__(self):
        return self.name

class Branch(ModelBase):
    district = models.ForeignKey(
        null=False,
        to='District',
        on_delete=models.DO_NOTHING,
        db_column='id_district',
    )

    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    class Meta:
        db_table = 'branch'

    def __str__(self):
        return self.name

class Department(ModelBase):
    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    objects = managers.DepartmentManager()

    class Meta:
        db_table = 'department'

    def __str__(self):
        return self.name

class Supplier(ModelBase):
    name = models.CharField(
        null=False,
        max_length=64,
    )

    legal_document = models.CharField(
        null=False,
        max_length=20,
        unique=True
    )

    class Meta:
        db_table = 'supplier'

    def __str__(self):
        return self.name

class ProductGroup(ModelBase):
    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    commission_percentage = models.DecimalField(
        null=False,
        max_digits=5,
        decimal_places=2,
    )

    gain_percentage = models.DecimalField(
        null=False,
        max_digits=5,
        decimal_places=2,
    )

    class Meta:
        db_table = 'product_group'

    def __str__(self):
        return self.name

class Product(ModelBase):
    product_group = models.ForeignKey(
        null=False,
        to='ProductGroup',
        on_delete=models.DO_NOTHING,
        db_column='id_product_group',
    )

    supplier = models.ForeignKey(
        null=False,
        to='Supplier',
        on_delete=models.DO_NOTHING,
        db_column='id_supplier',
    )

    name = models.CharField(
        null=False,
        max_length=64,
        unique=True
    )

    cost_price = models.DecimalField(
        null=False,
        max_digits=16,
        decimal_places=2,
    )

    sale_price = models.DecimalField(
        null=False,
        max_digits=16,
        decimal_places=2,
    )

    class Meta:
        db_table = 'product'

    def __str__(self):
        return self.name

class SaleItem(ModelBase):
    sale = models.ForeignKey(
        null=False,
        to='Sale',
        on_delete=models.DO_NOTHING,
        db_column='id_sale',
    )

    product = models.ForeignKey(
        null=False,
        to='Product',
        on_delete=models.DO_NOTHING,
        db_column='id_product',
    )

    quantity = models.DecimalField(
        null=False,
        max_digits=16,
        decimal_places=3,
    )

    sale_price = models.DecimalField(
        null=False,
        default=0,
        max_digits=16,
        decimal_places=3
    )

    class Meta:
        db_table = 'sale_item'

class Sale(ModelBase):
    customer = models.ForeignKey(
        null=False,
        to='Customer',
        on_delete=models.DO_NOTHING,
        db_column='id_customer',
    )

    branch = models.ForeignKey(
        null=False,
        to='Branch',
        on_delete=models.DO_NOTHING,
        db_column='id_branch',
    )

    employee = models.ForeignKey(
        null=False,
        to='Employee',
        on_delete=models.DO_NOTHING,
        db_column='id_employee',
    )

    date = models.DateTimeField(
        null=False,
        auto_now_add=True,
    )

    objects = managers.SaleManager()

    class Meta:
        db_table = 'sale'

class Employee(ModelBase):
    class Gender(models.TextChoices):
        MALE = ('M', 'Male')
        FEMALE = ('F', 'Female')

    department = models.ForeignKey(
        null=False,
        to='Department',
        on_delete=models.DO_NOTHING,
        db_column='id_department',
    )

    district = models.ForeignKey(
        null=False,
        to='District',
        on_delete=models.DO_NOTHING,
        db_column='id_district',
    )

    marital_status = models.ForeignKey(
        null=False,
        to='MaritalStatus',
        on_delete=models.DO_NOTHING,
        db_column='id_marital_status',
    )

    name = models.CharField(
        null=False,
        max_length=64,
    )

    salary = models.DecimalField(
        null=False,
        max_digits=16,
        decimal_places=2,
    )

    admission_date = models.DateTimeField(
        null=False
    )

    birth_date = models.DateTimeField(
        null=False
    )

    gender = models.CharField(
        null=False,
        max_length=1,
        choices=Gender.choices,
    )

    objects = managers.EmployeeManager()

    class Meta:
        db_table = 'employee'

    def upgrade_salary(self, upgrade_percentage):
        return round(self.salary + (self.salary * (Decimal(upgrade_percentage) / 100)))

    def __str__(self):
        return self.name