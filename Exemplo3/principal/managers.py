from django.db.models import Manager, FloatField, ExpressionWrapper, F, Sum, Count, Value, IntegerField

class SaleManager(Manager):
    def total_saled_by_department(self, year: int, start_month: int, end_month: int):
        qs = self.get_queryset().annotate(
            soma=ExpressionWrapper(
                F('saleitem__quantity') * F('saleitem__product__sale_price'), output_field=FloatField()
            )
        ).filter(
            date__year=year,
            date__month__range=(start_month, end_month)
        ).values('employee__department__name').annotate(
            total=Sum('soma')
        ).values('employee__department__name', 'total')
        return qs

# Não utilizar queries muito simples
# Ex: models.Employee.objects.filter(salary__range=(1000,5000))
class EmployeeManager(Manager):
    def by_range_salary(self, start_salary, end_salary):
        from principal import models as core_models
        return  self.get_queryset().filter(
            gender=core_models.Employee.Gender.FEMALE,
            salary__range=(start_salary, end_salary)
        )

class DepartmentManager(Manager):
    def total_employee(self):
        return self.get_queryset().values('name').annotate(
            count=Count('employee__id')
        )

    def increment_id(self):
        return self.get_queryset().annotate(
            count=ExpressionWrapper(F('id') + Value(1), output_field=IntegerField())
        )