from rest_framework.routers import DefaultRouter
from principal import viewsets

router = DefaultRouter()
router.register('state', viewsets.StateViewSet)
router.register('city', viewsets.CityViewSet)
router.register('employee', viewsets.EmployeeViewSet)
router.register('department', viewsets.DepartmentViewSet)
router.register('marital_status', viewsets.MaritalStatusViewSet)
router.register('district', viewsets.DistrictViewSet)
router.register('long_time_task', viewsets.LongTimeTask, basename='long_time_task')


urlpatterns = router.urls