from principal import models
from django.db.models import Value, CharField, Sum, ExpressionWrapper, F, FloatField, Avg, Min, Max, Count, OuterRef, \
    Subquery, Exists
from django.db.models.functions import Cast, LPad, Upper
from django.db import connection

# Fazer uma consulta que retorne os funcionários que ganham entre 1000 e 5000 do sexo feminino;
def questao1():
    qs = models.Employee.objects.filter(gender=models.Employee.Gender.FEMALE)
    qs = qs.filter(salary__range=(1000, 5000))
    return qs

# Fazer uma consulta que retorne o nome do cliente o bairro onde ele mora;
def questao2(id):
    qs = models.Customer.objects.filter(pk=id).values('name', 'district__name')
    return qs

# Fazer uma consulta que retorne as cidades com a sigla dos estados;
def questao3():
    return models.City.objects.all().values('name', 'state__abbreviation')

# Fazer uma consulta que retorne os 10 funcionários mais bem pagos;
def questao4():
    qs = models.Employee.objects.order_by('-salary')[:10]

    for i in qs:
        print(f'{i.name} - {i.salary}')

# Fazer uma consulta que retorne os 10 clientes com melhor renda mensal e casados;
def questao5():
    qs = models.Customer.objects.filter(marital_status=2)
    qs = qs.order_by('-income')[:10]

    for i in qs:
        print(f'{i.name} - {i.income}')

def dez_maior_renda_mensal(id_marital_status):
    qs = models.Customer.objects.filter(marital_status=id_marital_status)
    qs = qs.order_by('-income')[:10]

    for i in qs:
        print(f'{i.name} - {i.income}')

def por_estado_civil():
    lista = models.MaritalStatus.objects.all()
    for item in lista:
        print(item.name)
        dez_maior_renda_mensal(item.id)

def lpad_uso():
    qs = models.Employee.objects.annotate(
        codigo=LPad(
            Cast('id', output_field=CharField()),
            5,
            Value('0')
        )
    ).values('codigo')

    return qs

def upper_uso():
    qs = models.Department.objects.annotate(novo_nome=Upper('name')).values('novo_nome')
    return qs

def expressionwrapper_uso():
    return models.Employee.objects.annotate(novo_salario=ExpressionWrapper(
        F('salary') * Value(0.1) + F('salary'), output_field=FloatField()
    )).values('salary', 'novo_salario')

def sum_uso():
    return models.Employee.objects.filter(gender=models.Employee.Gender.FEMALE).aggregate(
        _sum=Sum('salary'),
        _avg=Avg('salary'),
        _min=Min('salary'),
        _max=Max('salary'),
    )

def agrupamento1():
    return models.Employee.objects.values('gender').annotate(
        soma=Sum('salary')
    ).values('gender', 'soma')

def agrupamento2():
    return models.Employee.objects.values('department__name', 'gender').annotate(
        soma=Sum('salary')
    ).values('department__name', 'gender', 'soma')


# Total vendido por zona de cliente entre 2010 e 2015;
def questao6_1():
    subtotal = ExpressionWrapper(
        F('saleitem__quantity') * F('saleitem__product__sale_price'),
        output_field=FloatField()
    )
    qs = models.Sale.objects.annotate(
        subtotal=subtotal
    ).values('customer__district__zone__name').annotate(
        total=Sum('subtotal')
    ).filter(date__year__range=(2010, 2015)).values(
        'customer__district__zone__name',
        'total'
    )
    return qs

# Total vendido por zona de cliente entre 2010 e 2015;
def questao6_2():
    results = []
    sql = """
            SELECT zone.name, SUM((sale_item.quantity * product.sale_price)) AS total
            FROM sale
                     LEFT OUTER JOIN sale_item ON (sale.id = sale_item.id_sale)
                     LEFT OUTER JOIN product ON (sale_item.id_product = product.id)
                     INNER JOIN customer ON (sale.id_customer = customer.id)
                     INNER JOIN district ON (customer.id_district = district.id)
                     INNER JOIN zone ON (district.id_zone = zone.id)
            WHERE EXTRACT('year' FROM sale.date AT TIME ZONE 'UTC') BETWEEN 2010 AND 2015
            GROUP BY zone.name
        """

    with connection.cursor() as cursor:
        cursor.execute(sql)
        results = cursor.fetchall()
    return results

# Total vendido por departamento no ano de 2010 entre o mês de janeiro e junho;
def questao7():
    qs = models.Sale.objects.annotate(
        soma=ExpressionWrapper(
            F('saleitem__quantity') * F('saleitem__product__sale_price'), output_field=FloatField()
        )
    ).filter(
        date__year=2010,
        date__month__range=(1, 6)
    ).values('employee__department__name').annotate(
        total=Sum('soma')
    ).values('employee__department__name', 'total')
    return qs

# Total que deve ser pago de comissão por grupo de produto no ano de 2016;
def questao8():
    commission = ExpressionWrapper(
        F('saleitem__product__sale_price') * F('saleitem__quantity') *
        (F('saleitem__product__product_group__commission_percentage') / Value(100)),
        output_field=FloatField()
    )
    queryset = models.Sale.objects.values('saleitem__product__product_group__name').annotate(
        commission=Sum(commission)
    ).filter(
        date__year=2016,
    ).values('saleitem__product__product_group__name', 'commission')
    return queryset

# Quantidade de clientes por zona;
def questao9():
    queryset = models.Customer.objects.values('district__zone__name').annotate(
        quantity=Count('*')
    ).values('district__zone__name', 'quantity')
    return queryset

# Quantidade de cidades por estado;
def questao10():
    queryset = models.City.objects.values('state__name').annotate(
        quantity=Count('*')
    ).values('state__name', 'quantity')
    return queryset

# Retorna a data da última venda de cada produto
def questao18():
    subquery = models.Sale.objects.filter(
        saleitem__product=OuterRef('id')  # OuterRef = consulta mais externa. Pega o id do produto
    ).annotate(max_date=Max('date')).values('max_date')
    subquery.query.group_by = None

    # subquery = models.Sale.objects.filter(
    #     saleitem__product=OuterRef('id')
    # ).order_by('-date').values('date')[:1]

    queryset = models.Product.objects.annotate(
        last_sale=Subquery(subquery)
    ).values('id', 'name', 'last_sale')
    return queryset

# Retorna só os funcionários que possuem alguma venda
def questao19():
    subquery = models.Sale.objects.filter(employee=OuterRef('id'))
    queryset = models.Employee.objects.annotate(
        has_sale=Exists(subquery)
    ).filter(has_sale=True).values('name')
    return queryset

# Retornar o departamento que nele não tenha homens, só mulheres.
def questao20():
    subquery = models.Employee.objects.filter(gender=models.Employee.Gender.MALE, department=OuterRef('id'))
    return models.Department.objects.annotate(_exists=Exists(subquery)).filter(_exists=False).values('name')
