--Script para resetar o sequence do id de cada tabela
do
$$
    declare
        query            record;
        max_id           integer;
        template_command varchar default 'select max(id) + 1 from %s';
        command_sequence varchar default 'alter sequence %s_id_seq restart %s';
    begin
        for query in select t.tablename
                     from pg_tables t
                     where t.schemaname = 'public'
                       and (not t.tablename ilike 'django%' and not t.tablename ilike 'auth%')
            loop
                execute format(template_command, query.tablename) into max_id;
                execute format(command_sequence, query.tablename, max_id);
            end loop;
    end;
$$

--Para atualizar o preco do produto em sale_item
update sale_item set sale_price = foo.sale_price
from (
    select si.id, p.sale_price from sale_item si
                                inner join product p on p.id = si.id_product
     ) as foo
where sale_item.id = foo.id;