# Generated by Django 4.1 on 2022-09-28 18:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0002_alter_categoria_nome_alter_cliente_estado_civil_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aluguel',
            options={'verbose_name_plural': 'alugueis'},
        ),
        migrations.AlterField(
            model_name='dvd',
            name='categoria',
            field=models.ForeignKey(db_column='id_categoria', on_delete=django.db.models.deletion.DO_NOTHING, related_name='categorias', to='principal.categoria'),
        ),
    ]
