from principal import models
from django.db.models import F, Value, ExpressionWrapper, FloatField, Case, When, CharField

def salario_10_porcento():
    calculo = ExpressionWrapper(
        F('salario') + (F('salario') * Value(0.10)),
        output_field=FloatField()
    )
    queryset = models.Funcionario.objects.annotate(
        salario_10_percent=calculo
    )
    return queryset

def update_salario_10_porcento():
    queryset = salario_10_porcento().update(salario=F('salario_10_percent'))
    return queryset

def utilizando_case():
    queryset = models.Funcionario.objects.annotate(
        sexo_descrito=Case(
            When(sexo=Value('M'), then=Value('Masculino')),
            default=Value('Feminino'),
            output_field=CharField()
        )
    )
    return queryset