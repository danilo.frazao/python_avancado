from django.db import models

# Create your models here.

class ModelBase(models.Model):
    id = models.AutoField(
        null=False,
        primary_key=True,
    )

    ativo = models.BooleanField(
        null=False,
        default=True,
    )

    criado_em = models.DateTimeField(
        null=False,
        auto_now_add=True,
    )

    modificado_em = models.DateTimeField(
        null=False,
        auto_now=True,
    )

    class Meta:
        abstract = True
        managed = True

class Funcionario(ModelBase):
    class Sexo(models.TextChoices):
        MASCULINO = ('M', 'Masculino')
        FEMININO = ('F', 'Feminino')

    nome = models.CharField(
        null=False,
        max_length=104,
    )

    sexo = models.CharField(
        null=False,
        max_length=1,
        choices=Sexo.choices,
        default='M'
    )

    salario = models.DecimalField(
        null=False,
        max_digits=10,
        decimal_places=2,
        default=1200.00,
    )

    departamentos = models.ManyToManyField(to='Departamento', through='DepartamentoFuncionario')

    class Meta:
        db_table = 'funcionario'

    def __str__(self):
        return f'{self.id} - {self.nome}'

class Departamento(ModelBase):
    nome = models.CharField(
        null=False,
        max_length=40,
        unique=True
    )

    funcionarios = models.ManyToManyField(to='Funcionario', through='DepartamentoFuncionario')

    class Meta:
        db_table = 'departamento'

    def __str__(self):
        return f'{self.id} - {self.nome}'

class DepartamentoFuncionario(ModelBase):
    departamento = models.ForeignKey(
        null=False,
        to='Departamento',
        on_delete=models.DO_NOTHING,
        db_column='id_departamento',
    )

    funcionario = models.ForeignKey(
        null=False,
        to='Funcionario',
        on_delete=models.DO_NOTHING,
        db_column='id_funcionario',
    )

    class Meta:
        db_table = 'departamento_funcionario'
        unique_together = [
            ('departamento', 'funcionario',)
        ]

    def __str__(self):
        return f'{self.departamento.nome} - {self.funcionario.nome}'

class Cliente(ModelBase):
    class Sexo(models.TextChoices):
        MASCULINO = ('M', 'Masculino')
        FEMININO = ('F', 'Feminino')

    class EstadoCivil(models.TextChoices):
        SOLTEIRO = ('S', 'Solteiro')
        CASADO = ('C', 'Casado')
        DIVORCIADO = ('D', 'Divorciado')
        VIUVO = ('V', 'Viúvo')

    nome = models.CharField(
        null=False,
        max_length=104,
    )

    sexo = models.CharField(
        null=False,
        max_length=1,
        choices=Sexo.choices
    )

    estado_civil = models.CharField(
        null=False,
        max_length=1,
        choices=EstadoCivil.choices
    )

    class Meta:
        db_table = 'cliente'

    def __str__(self):
        return f'{self.id} - {self.nome}'

class Aluguel(ModelBase):
    cliente = models.ForeignKey(
        null=False,
        to='Cliente',
        on_delete=models.DO_NOTHING,
        db_column='id_cliente',
    )

    funcionario = models.ForeignKey(
        null=False,
        to='Funcionario',
        on_delete=models.DO_NOTHING,
        db_column='id_funcionario',
    )

    class Meta:
        db_table = 'aluguel'
        verbose_name_plural = 'alugueis'

    def __str__(self):
        return f'{self.id} - {self.cliente.nome} - {self.funcionario.nome}'

class ItemAluguel(ModelBase):
    aluguel = models.ForeignKey(
        null=False,
        to='Aluguel',
        on_delete=models.CASCADE,
        db_column='id_aluguel',
    )

    dvd = models.ForeignKey(
        null=False,
        to='Dvd',
        on_delete=models.DO_NOTHING,
        db_column='id_dvd',
    )

    quantidade = models.IntegerField(
        null=False,
    )

    preco = models.DecimalField(
        null=False,
        max_digits=10,
        decimal_places=2,
    )

    class Meta:
        db_table = 'item_aluguel'

    def __str__(self):
        return f'{self.aluguel.id} - {self.dvd.descricao}'

    @property
    def subtotal(self):
        return self.quantidade * self.preco

class Categoria(ModelBase):
    nome = models.CharField(
        null=False,
        max_length=104,
        unique=True,
    )

    class Meta:
        db_table = 'categoria'

    def __str__(self):
        return f'{self.id} - {self.nome}'

class Dvd(ModelBase):
    categoria = models.ForeignKey(
        null=False,
        to='Categoria',
        on_delete=models.DO_NOTHING,
        db_column='id_categoria',
        related_name='dvds',
    )

    descricao = models.CharField(
        null=False,
        max_length=40,
        unique=True
    )

    preco = models.DecimalField(
        null=False,
        max_digits=10,
        decimal_places=2,
    )

    class Meta:
        db_table = 'dvd'

    def __str__(self):
        return f'{self.id} - {self.descricao}'
