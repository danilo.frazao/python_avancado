from django.contrib import admin
from principal import models

@admin.register(models.Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'sexo', 'estado_civil']
    list_per_page = 10
    search_fields = ['nome']
    list_filter = ['sexo']

class DepartamentoFuncionarioInline(admin.TabularInline):
    model = models.DepartamentoFuncionario

@admin.register(models.Funcionario)
class FuncionarioAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'ativo', 'get_departamentos']
    list_per_page = 10
    search_fields = ['nome']
    inlines = [DepartamentoFuncionarioInline]

    def get_departamentos(self, funcionario):
        departamentos = funcionario.departamentos.values_list('nome', flat=True)
        return ' - '.join(departamentos)

@admin.register(models.Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'ativo']
    list_per_page = 10
    search_fields = ['nome']

@admin.register(models.Dvd)
class DvdAdmin(admin.ModelAdmin):
    list_display = ['id', 'descricao', 'categoria', 'preco', 'ativo']
    list_per_page = 4
    search_fields = ['descricao']

class ItemAluguelAdmin(admin.TabularInline):
    model = models.ItemAluguel

@admin.register(models.Aluguel)
class AluguelAdmin(admin.ModelAdmin):
    list_display = ['id', 'cliente', 'funcionario']
    inlines = [ItemAluguelAdmin]

@admin.register(models.Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'ativo']
    search_fields = ['nome']