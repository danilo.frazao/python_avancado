from django.contrib import admin
from principal import models

# Register your models here.

@admin.register(models.Cargo)
class CargoAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'modificado_em', 'criado_em']
    list_per_page = 2
    search_fields = ['nome']

@admin.register(models.Funcionario)
class FuncionarioAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'sexo']
    list_per_page = 2
    search_fields = ['nome']