from django.db import models

# Create your models here.

class ModelBase(models.Model):
    id = models.AutoField(
        null=False,
        primary_key=True,
    )

    ativo = models.BooleanField(
        null=False,
        default=True,
    )

    criado_em = models.DateTimeField(
        null=False,
        auto_now_add=True,
    )

    modificado_em = models.DateTimeField(
        null=False,
        auto_now=True,
    )

    class Meta:
        abstract = True
        managed = True

class Cargo(ModelBase):
    nome = models.CharField(
        null=False,
        max_length=104,
    )

    class Meta:
        db_table = 'cargo'

    def __str__(self):
        return f'{self.id} - {self.nome}'

class Funcionario(ModelBase):
    class Sexo(models.TextChoices):
        MASCULINO = ('M', 'Masculino')
        FEMININO = ('F', 'Feminino')

    cargo = models.ForeignKey(
        null=False,
        to='Cargo',
        on_delete=models.DO_NOTHING,
        db_column='id_cargo',
    )

    nome = models.CharField(
        null=False,
        max_length=104,
    )

    sexo = models.CharField(
        null=False,
        max_length=1,
        choices=Sexo.choices
    )

    salario = models.DecimalField(
        null=False,
        max_digits=10,
        decimal_places=2,
    )

    class Meta:
        db_table = 'funcionario'