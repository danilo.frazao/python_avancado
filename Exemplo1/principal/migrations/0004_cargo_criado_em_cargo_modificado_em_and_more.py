# Generated by Django 4.1 on 2022-09-27 01:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0003_cargo_ativo_funcionario_ativo'),
    ]

    operations = [
        migrations.AddField(
            model_name='cargo',
            name='criado_em',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cargo',
            name='modificado_em',
            field=models.DateField(auto_now=True),
        ),
        migrations.AddField(
            model_name='funcionario',
            name='criado_em',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='funcionario',
            name='modificado_em',
            field=models.DateField(auto_now=True),
        ),
    ]
